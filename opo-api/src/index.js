
// import routes from './routes';

// /** Set username globally. */
// const { DB_NAME: database, DEV_USERNAME: devUsername } = process.env;

// request.username = username || devUsername;

// mount({ ...routes, database: 'Web_JLC_APPS' });

/**
 * Mount uses the routes to create a simple api structure.
 * Each route is pointed to the [process]{@link http://192.168.100.11/lite/modules/_process_.html} util.
 */



// mount(routes);

import { mount, request, send, username } from '@kyd/lite';
import routes from './routes';

const { DB_NAME: database, DEV_USERNAME: devUsername } = process.env;

// send({database: process.env });

request.username = username || devUsername;

mount( routes, { database });