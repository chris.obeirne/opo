import { select, updateElseInsert } from '@kyd/lite';

const generateShortId = (id) => {
  const DATABASE = 'Testing';
  const TABLE = 'Test_Short_ID';

  const options = {
    filter: [{
      key: 'id',
      equals: id,
    }],
    autoId: false,
  };

  const [row = {}] = select(DATABASE, TABLE, options).data;

  const newShortId = (row.shortId || 0) + 1;

  updateElseInsert(DATABASE, TABLE, {
    id,
    shortId: newShortId,
  }, options);

  return (newShortId).toString(36);
};

export default generateShortId;
