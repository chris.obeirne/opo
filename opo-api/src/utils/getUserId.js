import { select } from '@kyd/lite';
import getUsername from './getUsername';

/**
 * Get the user id by username.
 *
 * @param {string} username The username to lookup. Defaults to the current
 * logged in user.
 *
 * @returns {string|null} The user id as a `string` or `null` if the user id could not be
 * found.
 */
export default function getUserId(username = getUsername()) {
  // Get user ID by username
  const { data: userData } = select(process.env.DB_NAME, 'MMF_Users', {
    columns: [
      { column: 'ID', key: 'id' },
      { column: 'Username', key: 'username' },
    ],
    filter: [
      { key: 'username', equals: username },
    ],
  });

  if (userData.length < 1) {
    return null;
  }

  return userData[0].id;
}
