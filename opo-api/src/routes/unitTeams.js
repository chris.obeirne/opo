/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from '@kyd/lite';

// import { getUsername, getUserId } from '../utils/index.js';


const table = 'OPO_UnitTeams';
const database = 'Web_JLC';
// const j = 'JLU(SQ)';
export default {
  table,
  database,
  methods: ['GET',],
  // methods:['GET'],

preprocess({ body, method, query, username, ...leftOvers }) {
  //  response.send(leftOvers);
    if (method === 'GET') {
      const pg = query.page;
      const ipp = query.itemsPerPage;
      const srt = query.sort;
      query.filter = [ ...(query.filter || []), ];
      query.sort = srt;
      query.page = pg;
      query.itemsPerPage = ipp; 
      return { method, query, username, ...leftOvers };
    }
  },
// postprocess({data, method, query}) {
//     if (method === 'GET') {
//       const columns = getColumns(query.database, query.table);
//       data.meta.columns = columns;
//       // send(request);
//     } 
//   },

};
