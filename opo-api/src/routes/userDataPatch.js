/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from '@kyd/lite';

// import { getUsername, getUserId } from '../utils/index.js';

const table = 'Web_JLC.dbo.MMF_Users_Settings';
// const j = 'JLU(SQ)';

export default {
  table,
  methods: ['PATCH'],
  // methods:['GET'],

  preprocess(request) {
    const ppf = request.query.filter;
     const exists = select(process.env.DB_NAME, 'MMF_Users_Settings', { filter: request.query.filter });
     if (exists !== undefined && exists.data.length === 0) {
      const idv = ppf[0].equals;
      const newUser = [{
        id: idv,
        darkMode: true,
        mm: false,
        isAdmin: false,
      }];
      insertInto(
            process.env.DB_NAME,
            'MMF_Users_Settings',
            newUser,
          );
     }

if (request.method === 'PATCH') {
      request.body = request.body;
      // const recId = request.body.Id;
      request.filter = ppf;

      // send({
      //   text: 'phasing updated', 
      //   recId,
      //   fil: request.filter,
      //   bdy: request.body,
      // });
      // return { method, query, username, ...leftOvers };
    }
  },

  // postprocess({ data, method, query }) {
  //   if (method === 'GET') {

  //     // send(request);
  //   }
  // },

};
