import analytics from './analytics';
import poData from './poData';
import reportPOData from './reportPOData';
import reportAllPOData from './reportAllPOData';
import unitTeams from './unitTeams';


/**
 * This is where the endpoint structure gets created.
 * For example the export `test` will become the endpoint `/test`.
 */
export default {
  analytics,
  poData,
  unitTeams,
  reportPOData,
  reportAllPOData,
};
