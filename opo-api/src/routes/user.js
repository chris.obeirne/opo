import {
  generateId,
  send,
  insertInto,
  updateElseInsert,
  update,
  request,
  username,
  select,
  deleteFrom,
} from '@kyd/lite';

// import getUsername from '../utils/getUsername';
// import getUserId from '../utils/getUserId';
import { getUsername, getUserId } from '../utils/index.js';

export default {
  table: 'Web_JLC.dbo.MMF_Users',
  methods: ['GET', 'PATCH', 'DELETE'],
  validate() {
    // TODO: Validate data coming from client side
    return true;
  },

  secure({ query }) {
    // send('here');
    // Override filters coming from the client so that this endpoint only returns
    // the user row for the currently logged in DRN user

    // eslint-disable-next-line no-param-reassign
    return true;
  },
  // preprocess hook runs third
  preprocess({ query, method }) {
    const userId = getUserId();
  //   // if (method === 'DELETE') {
  //   //   send({ query });
  //   // }
  //   if (!query.developerAction) {
      query.filter = [{
        key: 'id',
        equals: userId,
      }];
  //   }
  },

  postprocess({ data = [] }, val, { send }) {
    // const [user = {}] = data;
    const nm = getUsername();
    const idv = getUserId();

    
    // if (!val.query.developerAction) {
      const [user = {}] = data;
      let newUser;
    //   send({
    //   xx: user,
    // });
      if (!user.id) {
      //   // NOTE: val.username comes straight off of the DRN
      //   // MM_Users_All is actually all DRN users
      
        const user = select(process.env.DB_NAME, 'MMF_Users', { filter: [{ key: 'username', equals: val.username }] });
      //   send({
      //   un: val.username,
      //   db: user,
      // });
        if (user.data.length === 1) {
          newUser = user.data[0];
          const elseResult = updateElseInsert(
            process.env.DB_NAME,
            'MMF_Users_Settings',
            {
              ...newUser,
              searchable: `${newUser.name} ${newUser.username} ${newUser.email}`,
              darkMode: true,
              denseRows: false,
              visitCount: 1,
            },
            {
              filter: [{
                key: 'id',
                equals: idv,
              }],
              autoId: false,
            });
            // send('user.js/postprocess: Userdata length ===1');
        } else {
          send('user.js/postprocess: User not found');
        }
      } else {
        
        update(
          process.env.DB_NAME,
          'MMF_Users_Settings',
          { visitCount: data[0].visitCount + 1 },
          {
            filter: [{
              key: 'id',
              equals: user.id,
            }],
          },
        );
        // send('user.js/postprocess: updated');
        // send({
        //   id: user.id,
        //   u: user,
        // })
      }

      const Analytics_Table = 'MMF_Analytics';
      insertInto(
        process.env.DB_NAME,
        Analytics_Table,
        [{
          ...val,
          id: generateId(),
          userAgent: val.headers.userAgent,
          method: val.method,
          route: val.route,
          createdUserId: getUserId(),
          createdDate: new Date(),
        }],
      );

      send(newUser || user);
    // }
  },
};