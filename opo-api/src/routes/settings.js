import {
  isSchema,
  isString,
  isOptional,
  isBoolean,
  isValidNumber,
  isEveryValid,
  isLessThan,
  isIn,
  isLengthLessThan,
  updateElseInsert,
  send,
} from '@kyd/lite';

const TABLE = 'MMF_Users_Settings';
const DATABASE = 'Web_JLC';

/**
 * The endpoint will become `/settings` based on the file name and export
 * from `index.js`.
 *
 * You will need to set the `original-url-override` request header to
 * `/settings` to request from this endpoint.
 */
export default {
  database: DATABASE,
  table: TABLE,
  methods: ['PATCH'],
  // validate: isSchema({
  //   body: isSchema({
  //     /* Sets the app to dark mode. */
  //     themeDarkMode: isOptional(isBoolean),
  //     /* Sets the drawer to dense mode. */
  //     drawerDenseMode: isOptional(isBoolean),
  //     /* Sets the primary colour of the app. */
  //     themePrimaryColor: isEveryValid([isString, isLengthLessThan(10)]),
  //     /* Sets the table density (dense, normal or sparse). */
  //     tableDensity: isOptional(isIn(['dense', 'normal', 'sparse'])),
  //     /* Rows per page. */
  //     tableItemsPerPage: isOptional(isEveryValid([isString, isLessThan(255)])),
  //     /* Home Page. */
  //     defaultHomePage: isOptional(isString),
  //     /* Subscribed to in-app notifications. */
  //     notifications: isOptional(isBoolean),
  //     /* Subscribed to email notifications. */
  //     notificationsEmail: isOptional(isBoolean),
  //     /* Receive an email notification summary every x minutes, 0 for instant. */
  //     notificationsEmailFrequency: isOptional(isValidNumber),
  //   }),
  // }),
  preprocess: ({ body = {}, id }) => {
    const results = updateElseInsert(DATABASE, TABLE, {
      ...body,
    }, {
      filter: [{
        key: 'id',
        equals: id,
      }],
    });
    send(results);
  },
};
