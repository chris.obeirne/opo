/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  pick,
  select,
  getColumns,
  execute,
  deleteFrom,
} from '@kyd/lite';
const table = 'PO_OutstandingPurchaseOrdersView';
const database = 'Web_JLC';


export default {
  table,
  database,
  methods: ['GET'],
  preprocess({
    body, method, query, username, ...leftOvers
  }) {
    if (method === 'GET') {
      const pg = query.page;
      const ipp = query.itemsPerPage;
      const srt = query.sort;
      query.filter = [...(query.filter || [])];
      query.sort = srt;
      query.page = pg;
      query.itemsPerPage = ipp;
    }

  },
  postprocess(data, val, { send }) {
    data.data = data.data.map(v => ({
      unit: v.unit,
      dstrctCode: v.dstrctCode,
      whouseId: v.whouseId,
      poNo: v.poNo,
      poItemNo: v.poItemNo,
      poType: v.poType,
      receivingRptNarrativeText: v.receivingRptNarrativeText,
      preqStkCode: v.preqStkCode,
      itemName: v.itemName,
      qtyOutstanding: v.qtyOutstanding,
      lineValue: v.lineValue,
      sio: v.sio,
      pdS: v.pdS,
      creationDate: v.creationDate,
      currDueDate: v.currDueDate,
      daysPastDuedate: v.daysPastDuedate,
      dueDateAgeRange: v.dueDateAgeRange,
      purchOfficer: v.purchOfficer,
      teamId: v.teamId,
      supplierNo: v.supplierNo,
      supplierName: v.supplierName,
      inspectCode: v.inspectCode,
      currNetPrI: v.currNetPrI,
      invoiceRef: v.invoiceRef,
      onstRcptDate: v.onstRcptDate,
      prcCde: v.prcCde,
      stockType: v.stockType,
      accc: v.accc,
      wbs: v.wbs,
      fund: v.fund,
      delivLocation221: v.delivLocation221,
      dateExtract: v.dateExtract
    }))
  },

};
