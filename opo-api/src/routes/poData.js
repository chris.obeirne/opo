/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import {
  generateId,
  send,
  get,
  insertInto,
  updateElseInsert,
  update,
  request,
  // username,
  select,
  getColumns,
  execute,
  deleteFrom,
} from '@kyd/lite';
const table = 'Web_JLC.dbo.PO_OutstandingPurchaseOrdersView';


export default {
  table,
  methods: ['GET'],
  // columns: [{column: 'jluId', hide: true}],
  preprocess({
    body, method, query, username, ...leftOvers
  }) {
    if (method === 'GET') {
      // const fySQL = (query.fy === null) ? ` (fy='2223') ` : ` (fy = '${query.fy}') `;
      const jluSQL = (query.jlu === null || query.jlu === undefined || query.jlu === '') ? ` (Unit Like '%') ` : ` (Unit = '${query.jlu}') `;
      
      const tS = query.teamSel;
      let tSSQL = ''
      if (tS!=='' && tS!== null && tS!==undefined  ) {
        tSSQL = ` AND (TEAM_ID IN ${tS}) `
      }
      const fmsIncl = query.fmsIncl;
      let fmsSQL = '';
      switch(fmsIncl) {
  case 'exFMS':
    fmsSQL = ` AND (SUPPLIER_NO <> 'FMS') `;
    break;
  case 'incFMS':
    fmsSQL = '';
    break;
    case 'justFMS':
    fmsSQL = ` AND (SUPPLIER_NO = 'FMS') `;
    break;
  default:
    fmsSQL = '';
}
      
      
      // const dsaSQL = (query.dsa === null || query.dsa === undefined) ? '' : `AND (dsa = '${query.dsa}') `;
      const SQL = `SELECT TOP (100) PERCENT 
      Unit as unit, 
      DSTRCT_CODE as dstrctCode, 
      CASE WHEN DATEDIFF(d, CURR_DUE_DATE, GETDATE()) >= 365 THEN '4. Year' 
          WHEN DATEDIFF(d, CURR_DUE_DATE, GETDATE()) >= 180 AND DATEDIFF(d, CURR_DUE_DATE, GETDATE()) < 365 THEN '3. Six Months' 
          WHEN DATEDIFF(d, CURR_DUE_DATE, GETDATE()) < 180 AND DATEDIFF(d, CURR_DUE_DATE, GETDATE()) > 90 THEN '2. Three Months' 
          ELSE '1. Current' END AS dueDateAgeRange, 
           SUM(1) AS pOCnt, 
           SUM(LineValue) AS pOVal, 
           CONVERT (varchar(11), dateExtract, 106) AS dateExtract
FROM   Web_JLC.dbo.PO_OutstandingPurchaseOrders
WHERE ${jluSQL} ${fmsSQL} ${tSSQL}
GROUP BY Unit, 
CASE WHEN DATEDIFF(d, CURR_DUE_DATE, GETDATE()) >= 365 THEN '4. Year' 
    WHEN DATEDIFF(d, CURR_DUE_DATE, GETDATE()) >= 180 AND DATEDIFF(d, CURR_DUE_DATE, GETDATE()) < 365 THEN '3. Six Months' 
    WHEN DATEDIFF(d, CURR_DUE_DATE, GETDATE()) < 180 AND DATEDIFF(d, CURR_DUE_DATE, GETDATE()) 
           > 90 THEN '2. Three Months' ELSE '1. Current' END, DSTRCT_CODE,
           CONVERT (varchar(11), dateExtract, 106) 
      `;
      // where ${fySQL} ${dsaSQL} 
     
   const poData = execute(SQL);
        send({ 
          data: poData,
          sql: SQL,
          });

     }
  },
};
