import {
  generateId,
  send,
  insertInto,
  updateElseInsert,
  update,
  request,
  username,
  select,
  deleteFrom,
} from '@kyd/lite';

// import getUsername from '../utils/getUsername';
// import getUserId from '../utils/getUserId';
import { getUsername, getUserId } from '../utils/index.js';

export default {
  table: 'SCBD_Users',
  methods: ['GET'],

  postprocess(data, val, { send }) {
    data.data = data.data.map(o => ({
      ...o,
      disabled: o.username === val.username,
    }))
    //  send(result);
    // send(dasta);
  }
}