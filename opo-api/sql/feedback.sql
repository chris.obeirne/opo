USE [Testing]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Test_Feedback](
	/* Keeps the record unique. */
	[ID] [uniqueidentifier] NOT NULL,
	/* The feedback type from the user (categorical). */
	[Feedback_Type_ID] [varchar](max) NOT NULL,
	/* Track the browser type for the feedback that was sent. */
	[User_Agent] [varchar](max) NOT NULL,
	/* The full URL or path (including the query string) on the browser at the
	time when the feedback was sent. */
	[URL] [varchar](max) NOT NULL,
	/* The document reference point for the feedback if it was triggered from a
	specific place. */
	[Reference] [varchar](max) NOT NULL,
	/* The description of the feedback from the user. */
	[Description] [varchar](max) NOT NULL,
	/* If the feedback required resolution, has it been resolved? */
	[Resolved] [bit] NULL,
	/* The viewport (size) of the browser window. */
	[Event_Viewport_Resolution] [varchar](1000) NULL,
	/* The username of the person who triggered the event. If the user wished to
	remain anonymous this field will be blank. */
	[Created_Username] [varchar](max) NULL,
	/* The date the event occured. */
	[Created_Date] [smalldatetime] NOT NULL,
	PRIMARY KEY (ID)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


