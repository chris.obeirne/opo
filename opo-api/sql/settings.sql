USE [Testing]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Test_Settings](
	/* Keeps the record unique. */
	[ID] [uniqueidentifier] NOT NULL,
	/* The username of the persons settings. */
	[Username] [varchar](max) NOT NULL,
	/* Sets the app to dark mode. */
	[Theme_Dark_Mode] [bit] NULL,
	/* Sets the drawer to be dense. */
	[Drawer_Dense_Mode] [bit] NULL,
	/* Sets the primary colour of the app. */
	[Theme_Primary_Color] [varchar](9) NULL,
	/* Sets the table density (dense, normal or sparse). */
	[Table_Density] [varchar](6) NULL,
	/* Rows per page. */
	[Table_Items_Per_Page] [tinyint] NULL,
	/* Home Page. */
	[Default_Home_Page] [varchar](max) NULL,
	/* Subscribed to in-app notifications. */
	[Notifications] [bit] NULL,
	/* Subscribed to email notifications. */
	[Notifications_Email] [bit] NULL,
	/* Receive an email notification summary every x minutes, 0 for instant. */
	[Notifications_Email_Frequency] [int] NULL,
	/* The date the settings were last modified. */
	[Modified_Date] [smalldatetime] NOT NULL,
	PRIMARY KEY (ID)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


