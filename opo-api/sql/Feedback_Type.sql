USE [Testing]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Test_Feedback_Types](
  /* Keeps the record unique, links to Feedback table. */
	[ID] [uniqueidentifier] NOT NULL,
  /* The type of feedback. */
	[Type] [varchar](50) NOT NULL
) ON [PRIMARY]
GO

INSERT [dbo].[Test_Feedback_Types] ([ID], [Type]) VALUES (N'9aebbf75-cd92-4d96-a293-49e87abe4bc6', N'I need help')
GO
INSERT [dbo].[Test_Feedback_Types] ([ID], [Type]) VALUES (N'b2989ac5-cbe7-40b4-a05a-b0054a4aace9', N'I have a suggestion/feedback')
GO
INSERT [dbo].[Test_Feedback_Types] ([ID], [Type]) VALUES (N'26260def-fd9d-4012-9e53-2e86345a8521', N'I have a technical issue')
GO
INSERT [dbo].[Test_Feedback_Types] ([ID], [Type]) VALUES (N'825f53e5-4d28-4860-bd7b-7a3c810706ec', N'I need access')
GO
