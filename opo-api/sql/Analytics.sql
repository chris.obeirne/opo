USE [Testing]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Test_Analytics](
	/* Keeps the record unique. */
	[ID] [uniqueidentifier] NOT NULL,
	/* Track the browser type when the event occurred. */
	[User_Agent] [varchar](max) NOT NULL,
	/* The full URL or path (including the query string) on the browser at the
	time when the event occurred. */
	[URL] [varchar](max) NOT NULL,
	/* The page title at the time when the event occurred. */
	[Title] [varchar](max) NOT NULL,
	/* The event.type. */
	[Event_Type] [varchar](max) NOT NULL,
	/* The event.path (dom path) where the event occurred. */
	[Event_Path] [varchar](max) NOT NULL,
	/* The viewport (size) of the browser window. */
	[Event_Viewport_Resolution] [varchar](1000) NULL,
	/* The x coordinate where the event occurred. */
	[Event_X] [int] NULL,
	/* The y coordinate where the event occurred. */
	[Event_Y] [int] NULL,
	/* The key code of the event for keyboard based events. */
	[Event_Key_Code] [int] NULL,
	/* The is composing flag of the event for keyboard based events. */
	[Event_Is_Composing] [bit] NULL,
	/* The event value if the event has one. */
	[Event_Value] [int] NULL,
	/* The username of the person who triggered the event. */
	[Created_Username] [varchar](max) NULL,
	/* The date the event occured. */
	[Created_Date] [smalldatetime] NOT NULL,
	PRIMARY KEY (ID)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


