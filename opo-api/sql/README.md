# SQL
Provides initialisation for tables that provide general functionality in new apps.

## Usage
Change `Testing` to the main database of your app. Change `Test` to the namespace of your app.