/* eslint-disable no-unused-vars */
import Vue from 'vue';
import VueRouter from 'vue-router';
// import dash from '@/components/dash';
// import DeveloperTools from '@/views/DeveloperTools';
import Chart from '../views/Chart';

import vuetify from '../plugins/vuetify';
import store from '../store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Chart',
    component: Chart,
  },
  // PhasingSummary
  // {
  //   path: '/',
  //   name: 'PhasingSummary',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/PhasingSummary'),
  // },
  // {
  //   path: '/PhasingSummary',
  //   name: 'PhasingSummary',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/PhasingSummary'),
  // },
  // {
  //   path: '/jluData',
  //   name: 'jluData',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/jluData'),
  // },
  // {
  //   path: '/fyData',
  //   name: 'fyData',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/fyData'),
  // },
  // {
  //   path: '/dsaData',
  //   name: 'dsaData',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/dsaData'),
  // },
  // {
  //   path: '/userData',
  //   name: 'userData',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/userData'),
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About'),
  // },
];

const router = new VueRouter({
  routes,
});

router.beforeEach(({ name }, _, next) => {
  const { dispatch } = store;
  // dispatch('app/getUser').then((user = {}) => {
  //   // console.log('the user return', name, user)
  //   if (user.id) {
  //     vuetify.framework.theme.isDark = user.darkMode;
  //   } else {
  // console.log('first time user');
  //   }
  // });

  next();
});

export default router;
