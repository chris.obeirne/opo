export default (d, denom = ' ', shortYear) => {
  if (!d || d === null) return undefined;

  const date = new Date(d);

  const monthNames = [
    'Jan', 'Feb', 'Mar',
    'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec',
  ];

  const day = date.getDate();
  const monthIndex = date.getMonth();
  const year = date.getFullYear();

  return `${day > 9 ? day : `0${day}`}${denom}${monthNames[monthIndex]}${denom}${shortYear ? year.toString().substring(2, 4) : year}`;
};
