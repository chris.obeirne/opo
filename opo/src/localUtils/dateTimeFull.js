import { isDate, isNaN } from 'lodash';

const monthNames = [
  'Jan', 'Feb', 'Mar',
  'Apr', 'May', 'Jun', 'Jul',
  'Aug', 'Sep', 'Oct',
  'Nov', 'Dec',
];

function addZero(i) {
  if (i < 10) {
    // eslint-disable-next-line
      i = `0${i}`;
  }
  return i;
}

function adjHr(i) {
  let adj = i;
  if (adj > 12) {
    // eslint-disable-next-line
      adj = adj - 12;
  }
  if (adj < 10) {
    // eslint-disable-next-line
      adj = `0${adj}`;
  }
  return adj;
}

function amPm(i) {
  const ap = (i >= 12) ? 'pm' : 'am';
  return ap;
}
// const test = new Date('2020-09-15T00:23:22.000Z');
const dateTimeFull = (val) => {
  let dte = new Date(val);
  const isValidDate = isDate(dte) && !isNaN(dte.valueOf());
  let r;
  if (isValidDate) {
    // console.log('the passed in dt', dte);
    // const z = dte.getTimezoneOffset() * 60 * 1000;
    // console.log('the z is ', z);
    // dte -= z;
    dte = new Date(dte);
    // console.log('the adj is ', dte);
    const day = addZero(dte.getDate());
    const monthIndex = dte.getMonth();
    const year = dte.getFullYear();
    const hr = adjHr(dte.getHours());
    const min = addZero(dte.getMinutes());
    const sec = addZero(dte.getSeconds());
    const ampm = amPm(dte.getHours());

    r = `${day} ${monthNames[monthIndex]} ${year} ${hr}:${min}:${sec} ${ampm}`;
  }
  return r;
};

export default dateTimeFull;
