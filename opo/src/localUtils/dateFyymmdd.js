import {
  isDate,
  isNaN,
} from 'lodash';

const dateFddmmmyy = (val) => {
  let dte = null;
  if (val !== '' && val !== null) {
    dte = new Date(val);
    const isValidDate = isDate(dte) && !isNaN(dte.valueOf());
    if (isValidDate) {
      const z = dte.getTimezoneOffset() * 60 * 1000;
      dte -= z;
      dte = new Date(dte);
      let day = dte.getDate();
      day = (Number(day) < 10) ? `0${day}` : day;
      let monthIndex = dte.getMonth();
      monthIndex = Number(monthIndex) + 1;
      monthIndex = (monthIndex < 10) ? `0${monthIndex}` : monthIndex;
      let year = dte.getFullYear();
      year = year.toString().substring(2, 4);
      dte = `${year}${monthIndex}${day}`;
    } else {
      dte = 'Unknown Date';
    }
  }
  return dte;
};

export default dateFddmmmyy;
