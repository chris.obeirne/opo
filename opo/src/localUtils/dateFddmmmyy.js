import {
  isDate,
  isNaN,
} from 'lodash';

const dateFddmmmyy = (val) => {
  // let dte = (val === '' || val === null) ?
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  let dte = null;
  if (val !== '' && val !== null) {
    dte = new Date(val);
    const isValidDate = isDate(dte) && !isNaN(dte.valueOf());
    // let r;
    if (isValidDate) {
    // const d = new Date();
    // let dte = new Date(val);
      const z = dte.getTimezoneOffset() * 60 * 1000;
      dte -= z;
      dte = new Date(dte);
      // console.log(` monthIndex ${monthIndex} and day ${day} year ${year}`);

      const day = dte.getDate();
      const monthIndex = dte.getMonth();
      let year = dte.getFullYear();
      year = year.toString().substring(2, 4);
      dte = `${day} ${months[monthIndex]} ${year}`;
    // r = `${day} ${monthNames[monthIndex]} ${year}`;
    } else {
      dte = 'Unknown Date';
    }
  }
  return dte;
};

export default dateFddmmmyy;
