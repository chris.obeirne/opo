/* eslint-disable */
import {
  isString, isPlainObject, isArray,
} from 'lodash';
import { get, patch } from 'kyd-utils';

export default {
  state: {

    themePrimaryColors: [
      // The text value is the value given to the server to store in the database
      // We'll use it to resolve to a unique colour depending on whether the app
      // is in light mode or dark mode
      // The text value is also used in the settings page to populate the dropdown
      // TODO: Colours for dark vs light theme will be slightly different
      { dark: '#3F51B5', light: '#3F51B5', text: 'indigo' },
      { dark: '#F44336', light: '#F44336', text: 'red' },
      { dark: '#E91E63', light: '#E91E63', text: 'pink' },
      { dark: '#9C27B0', light: '#9C27B0', text: 'purple' },
      { dark: '#2196F3', light: '#2196F3', text: 'blue' },
      { dark: '#00BCD4', light: '#00BCD4', text: 'cyan' },
      { dark: '#009688', light: '#009688', text: 'teal' },
      { dark: '#4CAF50', light: '#4CAF50', text: 'green' },
      { dark: '#CDDC39', light: '#CDDC39', text: 'lime' },
      { dark: '#FFEB3B', light: '#FFEB3B', text: 'yellow' },
      { dark: '#FFC107', light: '#FFC107', text: 'amber' },
      { dark: '#FF9800', light: '#FF9800', text: 'orange' },
      { dark: '#795548', light: '#795548', text: 'brown' },
      { dark: '#9E9E9E', light: '#9E9E9E', text: 'grey' },
      { dark: '#999999', light: '#000000', text: 'inverted' },
    ],
    refreshSecondsSelection: [10, 20, 30, 40, 50, 60],
    vWH: {},
    user: {},
  },

  getters: {
    themePrimaryColors: ({ themePrimaryColors }) => themePrimaryColors,
    getvWH: (state) => state.vWH,
    drawer: (state) => state.drawer,
    user: (state) => state.user,
  },

  mutations: {
    SET_VWH(state, payload) {

      state.vWH = payload;
      // console.log('app VWH', state.vWH)
    },
    SET_DRAWER(state, payload) {
      state.drawer = payload;
    },
    SET_ERROR(state, payload) {
      if (isObject(payload)) {
        state.error = payload;
      } else {
        state.error = {};
      }
    },
    SET_USER(state, payload) {
      state.user = payload;
      console.log(payload)
    }
  },

  actions: {
    setViewPort: ({
      commit,
    }, payload) => {
      // eslint-disable-next-line
      // console.log(payload);
      commit('SET_VWH', payload);
    },
    setDrawer: ({
      commit,
    }, payload) => {
      // eslint-disable-next-line
      // console.log(payload);
      commit('SET_DRAWER', payload);
    },
    getUser: ({ commit }) => get('/user')
      .then(({ data }) => {
        // console.log('the user', data)
        commit('SET_USER', data);
        return data;
      }),
  },
};
