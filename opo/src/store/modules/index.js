import camelCase from 'lodash/camelCase';

const requireModule = require.context('.', false, /\.js$/);

export default requireModule.keys().reduce((modules, fileName) => (fileName !== './index.js'
  ? {
    ...modules,
    [camelCase(fileName.replace(/(\.\/|\.js)/g, ''))]: {
      namespaced: true,
      ...requireModule(fileName).default,
    },
  }
  : modules
), {});
