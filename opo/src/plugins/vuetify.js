import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: false,
    themes: {
      light: {
        // primary: colors.purple,
        // secondary: colors.grey.darken1,
        // accent: colors.shades.black,
        // error: colors.red.accent3,
        pageBackground: colors.grey.lighten1,
        tooltipBackground: colors.grey.base,
        cardBackground: colors.grey.lighten3,
      },
      dark: {
        // primary: colors.blue.lighten3,
        pageBackground: colors.grey.darken4,
        tooltipBackground: colors.grey.darken1,
        cardBackground: colors.grey.darken2,
      },
    },
  },
});
