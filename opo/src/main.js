import Vue from 'vue';
import VueMask from 'v-mask';
import HighchartsVue from 'highcharts-vue';
import highcharts from 'highcharts';

import dataHc from 'highcharts/modules/data';
import highchartsmore from 'highcharts/highcharts-more';
// import exportingInit from 'highcharts/modules/exporting';
import stockInit from 'highcharts/modules/stock';

import drilldown from 'highcharts/modules/drilldown';
import exportdata from 'highcharts/modules/export-data';
import VueCurrencyFilter from 'vue-currency-filter';
import { dateTimeFull, dateOnly, dateShortFromLong } from './localUtils';

import App from './App';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@mdi/font/css/materialdesignicons.css';

Vue.config.productionTip = false;
Vue.use(VueCurrencyFilter, {
  symbol: '$',
  thousandsSeparator: ',',
  fractionCount: 2,
  fractionSeparator: '.',
  symbolPosition: 'front',
  symbolSpacing: false,
});
Vue.use(VueMask);
Vue.use(HighchartsVue, {
  highcharts,
});
drilldown(highcharts);
stockInit(highcharts);
dataHc(highcharts);

highchartsmore(highcharts);
exportdata(highcharts);
// highcharts.wrap(highcharts.seriesTypes.pie.prototype, 'drilldown', function test(proceed) {
//   // const series = this.chart.series;
//   // eslint-disable-next-line
//   proceed.apply(this, Array.prototype.slice.call(arguments, 1));
//   this.chart.hcEvents.drilldown[0].fn(this.chart);
// });
Vue.filter('betterDate', (value) => {
  let ret = '';
  if (value) {
    ret = dateTimeFull(value);
  } else {
    ret = '';
  }
  return ret;
});

Vue.filter('shortDate', (value) => {
  let ret = '';
  if (value) {
    ret = dateOnly(value);
  } else {
    ret = '';
  }
  return ret;
});
Vue.filter('shortenedDate', (value) => {
  let ret = '';
  if (value) {
    ret = dateShortFromLong(value);
  } else {
    ret = '';
  }
  return ret;
});
new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
